#!/usr/bin/env bash

set -ex

VERDI_HOME=/home/gitlab-runner/verdi
source ${VERDI_HOME}/bin/activate
source $PWD/config.txt
export SKIP_PUBLISH="noskip"
isSet=1
# Check for CI/CD Variables
for var in S3_CODE_BUCKET MOZART_URL GRQ_REST_URL
do
    if [ -z "${!var}" ]; then
        echo "${var} not set"
        isSet=""
    fi
done
if [ -z "${isSet}" ]; then
    echo "One or more variable is not set"
    exit 1
fi
echo 'listing variables'
echo ${REPO_NAME}
echo ${BRANCH}
echo ${S3_CODE_BUCKET}
echo ${MOZART_URL}
echo ${GRQ_REST_URL}
echo ${SKIP_PUBLISH}
echo ${CONTAINER_REGISTRY}
CACHE_BUST=$(date +%s)
echo 'end of listing variables'
${VERDI_HOME}/ops/container-builder/build-container.bash.app-pack ${REPO_NAME} ${BRANCH} ${S3_CODE_BUCKET} ${MOZART_URL} ${GRQ_REST_URL} ${SKIP_PUBLISH} ${CONTAINER_REGISTRY} --build-arg BASE_IMAGE_NAME=${BASE_IMAGE_NAME} --build-arg REPO_URL_WITH_TOKEN=${REPO_URL_WITH_TOKEN} --build-arg REPO_NAME=${REPO_NAME} --build-arg BRANCH=${BRANCH} --build-arg BUILD_CMD="${BUILD_CMD}" --build-arg CACHE_BUST=${CACHE_BUST}
