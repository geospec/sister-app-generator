'''

SISTER OGC Application Package Wrapper

This is a project-specific OGC wrapper that invokes the reference implementation for 
OGC application package generation.

'''
import sys
import yaml
import json
import logging
import json
from ogc_app_pack import AppPackGenerator
import os

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(levelname)s - %(message)s',
    filename='app_pack.log'
)


def generate_config(data):
    config_filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "config.txt")
    print(f"Writing config file at {config_filepath}")
    with open(config_filepath, "w") as f:
        f.write(f"BASE_IMAGE_NAME={data.get('docker_url')}\n")
        f.write(f"REPO_URL_WITH_TOKEN={data.get('repository_url')}\n")
        f.write(f"REPO_NAME={data.get('algo_name')}\n")
        f.write(f"BRANCH={data.get('version')}\n")
        if data.get('build_command'):
            f.write(f"BUILD_CMD={data.get('build_command')}\n")
    print("Completed writing config file")


def main(args):
    if len(sys.argv) <= 1:
        logging.error("An input must be provided. Exiting.")
        return 1

    data = {}
    algo_file = args[1]
    logging.info("Generating application package using: %s", algo_file)

    print("Algo data:")
    print(algo_file)

    # TODO: determine if input is already cwl or not
    try:
        #data = yaml.safe_load(algo_file)
        data = json.loads(json.loads(algo_file))

        # OGC does not support file, positional inputs the way MAAP does, so we need to flatten the inputs.
        flattened_inputs = []
        for value in data['inputs'].values():
            print("test...")
            if len(value) != 0:
                for k in value:
                    flattened_inputs.append(k)
        data['inputs'] = flattened_inputs
    except Exception as e:
        logging.error("An error occurred: %s", str(e))
    
    # Generate the OGC package using the reference implementation
    print("Data for ogc:")
    print(data)
    app = AppPackGenerator()
    app.generate_process_cwl(data)
    generate_config(data)
    return 0

# def flatten_inputs(data):
#     print("flattening inputs...")
#     flattened_inputs = []
#     for value in data.values():
#         if len(value) != 0:
#             for k in value:
#                 flattened_inputs.append(k)
#     return flattened_inputs

if __name__ == '__main__':
    return_code = main(sys.argv)

